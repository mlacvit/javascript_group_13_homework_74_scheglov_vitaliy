const express = require('express');
const {nanoid} = require("nanoid");
const router = express.Router();
const fs = require('fs');
const date = new Date();
const id = nanoid();
const file = `./messages/${date.toISOString()}.txt`;
const path = ".messages/";
let data = [];

router.get('/?', (req, res) => {
  fs.readdir(path, (err, files) => {
    files.forEach(file => {
      try {
        res.send(data = fs.readFileSync(JSON.parse(file) + '.txt'));
      }catch (e){
        data = [];
      }
    });
  });
});

router.get('/:id', (req, res) => {
  const getFilesAll = fs.readFileSync(file);
  const fileOne = getFilesAll.find(p => p.id === req.params.id);
  res.send(fileOne);
});

router.post('/', (req, res) => {
  const message = {
    author: req.body.author,
    description: req.body.description,
    date: date.toISOString(),
    id,
  }
  data.push(message);
  const postFile = fs.writeFileSync(file, JSON.stringify(data));
  res.send((postFile));
});

module.exports = router;