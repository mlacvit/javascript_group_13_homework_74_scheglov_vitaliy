const express = require('express');
const app = express();
const messages = require('./mess');
const port = 8000;

app.use(express.json());
app.use('/mess/', messages);

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});

